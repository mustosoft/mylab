from django.urls import path, re_path

from . import views

urlpatterns = [
    #path('', views.index, name='index'),
    path('', views.index, name='view_jadwal'),
    path('jadwal/', views.view_jadwal, name='view_jadwal'),
    path('jadwal/add/', views.add, name='add_jadwal'),
	path('jadwal/edit/', views.edit, name='edit_jadwal'),
]
