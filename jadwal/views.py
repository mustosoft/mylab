from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.utils import timezone

from .models import Kegiatan
from .forms import JadwalForm

# Create your views here.

def index(request):
	context = {}
	return render(request, 'home.html', context)

def view_jadwal(request):
	list_jadwal = Kegiatan.objects.order_by('id')
	context = {'list_jadwal': list_jadwal}
	return render(request, 'view_data.html', context)

def add(request):
	form = JadwalForm(request.POST or None)

	if (request.method == 'POST'):
		response = {}
		response['nama_kegiatan'] = request.POST['nama_kegiatan'] if request.POST['nama_kegiatan'] != "" else "Untitled"
		response['waktu'] = request.POST['waktu'] if request.POST['waktu'] != "" else None
		response['tempat'] = request.POST['tempat'] if request.POST['tempat'] != "" else "N/A"
		response['kategori'] = request.POST['kategori'] if request.POST['kategori'] != "" else "N/A"

		jadwal = Kegiatan(nama_kegiatan = response['nama_kegiatan'],
				waktu = response['waktu'],
				tempat = response['tempat'],
				kategori = response['kategori'])
		#print(jadwal.objects.all().values())
		jadwal.save()
		return HttpResponseRedirect('/jadwal/')
	else:
		return render(request, 'add.html')

def edit(request):
	if (request.method == 'POST'):
		response = {}
		response['clear'] = request.POST['clear']

		if (response['clear'] == 'exec'):
			for k in Kegiatan.objects.all():
				k.delete()

		return HttpResponseRedirect('/jadwal/')
	else:
		return HttpResponseRedirect('/jadwal/')