from django.db import models
from django.utils import timezone
from datetime import datetime, date

class Kegiatan(models.Model):
	id = models.AutoField(primary_key=True)
	nama_kegiatan = models.CharField(max_length=30)
	waktu = models.DateTimeField()
	tempat = models.CharField(max_length=100)
	kategori = models.CharField(max_length=20)
	date_added = models.DateTimeField(default=timezone.now)
