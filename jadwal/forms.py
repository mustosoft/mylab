from django import forms

class JadwalForm(forms.Form):
	nama_kegiatan = forms.CharField(label='Nama Kegiatan', max_length=30)
	waktu = forms.DateTimeField()
	kategori = forms.CharField(label='Kategori', max_length=20)
	tempat = forms.CharField(label='Tempat', max_length=100)

'''
	id = models.AutoField(primary_key=True)
	nama_kegiatan = models.CharField(max_length=30)
	waktu = models.DateTimeField()
	tempat = models.CharField(max_length=100)
	kategori = models.CharField(max_length=20)
	date_added = models.DateTimeField(default=timezone.now)'''